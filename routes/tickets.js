// tickets.js

var TicketRecipe = require('../recipes/ticket');

var ticket = new TicketRecipe();

exports.createTicket = function(req, res) {
  if (!req.body.title.length) return res.json(400, 'Bad Request');

  ticket.newTicket(req.body, function(err, doc) {
    if (err) return res.json(500, 'Internal Server Error');
    if (doc === null) return res.json(409, 'Conflict');
    return res.json(201, doc);
  });
};

exports.tickets = function(req, res) {
  ticket.tickets(function(err, doc) {
    if (err) res.json(500, 'Internal Server Error');
    if (doc === null) return res.json(404, 'Not Found');
    return res.json(200, doc);
  });
};

exports.ticket = function(req, res) {
  var ticketId = req.params.tid;

  if (!ticketId)
    return res.json(400, 'Bad Request');

  ticket.ticket(ticketId, function(err, doc) {
    if (err) res.json(500, 'Internal Server Error');
    if (doc === null) return res.json(404, 'Not Found');
    return res.json(200, doc);
  });
};

exports.createNote = function(req, res) {
  var ticketId = req.params.tid;

  if (!ticketId)
    return res.json(400, 'Bad Request');

  ticket.createNote(ticketId, req.body, function(err, doc) {
    if (err) return res.json(500, 'Internal Server Error');
    if (doc === null) return res.json(404, 'Not Found');
    return res.json(201, doc);
  });
};

exports.editNote = function(req, res) {
  var ticketId = req.params.tid,
    noteId = req.params.nid;

  if (!(ticketId && noteId))
    return res.json(400, 'Bad Request');

  ticket.editNote(ticketId, noteId, req.body, function(err, doc) {
    if (err) return res.json(500, 'Internal Server Error');
    if (doc === null) return res.json(404, 'Not Found');
    return res.json(201, doc);
  });
};