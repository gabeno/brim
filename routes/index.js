
/*
 * GET home page.
 */

var check = require('validator').check,
  Customer = require('../models/customer'),
  mail = require('../util/mail'),
  smtpConfig = require('../settings.json').smtp;

exports.index = function(req, res) {
  res.render('main', {
    plans: true,
    thankyou: true,
    optIn: true
  });
};

exports.save = function(req, res) {
  var opts, sender,
    email = req.body.email.toLowerCase(),
    validEmail = check(email).isEmail();

  var html = [
    '<h2>Thanks for your interest in Brim</h2>',
    '<p>We have added you to our Brim waiting list. We will be sure to send you an invite soon.</p>',
    '<p>In the meantime, follow us on <a href="https://twitter.com/BrimApp">twitter</a> for updates. ',
    'We are very excited to get you using Brim as your helpdesk tool.</p>',
    '<p>Kind Regards,</p>',
    '<p><em>Gabriel M</em>, Brim Team</p>'
  ];

  if (!validEmail._errors.length) {
    // save email to db
    // send email message onlyfor new email
    // redirect to /thankyou page
    
    Customer.findOne({ email: email }, function(err, docs) {
      // email not in database
      if (!docs) {
        var newCustomer = new Customer({ email: email });
        // save email to db
        newCustomer.save(function(err) {
          if (err) return console.log('Unable to save user email');
          // new email, send an onboarding email message
          opts = {
            to: email,
            subject: 'Thank you Note',
            html: html.join(''),
            from: 'Brim Team <trybrim@gmail.com>',
            replyTo: 'trybrim@gmail.com',
            bcc: 'gmajivu@gmail.com',
            generateTextFromHTML: true
          };
          sender = mail.send(smtpConfig);
          sender.sendEmail(opts, function(err, res) {
            if (err) console.log(err);
            console.log('Message sent, %s', res.message);
          });
          res.send({ newEmail: true });
        });
      }
      // email already exists in database
      else {
        res.send({ newEmail: false });
      }
    });
  }
};

exports.plans = function(req, res) {
  res.render('main', {
    index: true,
    thankyou: true,
    optIn: true
  });
};

exports.choose = function(req, res) {
  res.render('main', {
    index: true,
    thankyou: true,
    plans: true
  });
};

exports.thankyou = function(req, res) {
  if (req.query.m) {
    res.render('main', {
      index: true,
      plans: true,
      optIn: true,
      emailExists: true
    });
  }
  else {
    res.render('main', {
      index: true,
      plans: true,
      optIn: true
    });
  }
};