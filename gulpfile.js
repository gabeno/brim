// gulp - a new build system for the project

var gulp = require('gulp'),
  jshint = require('gulp-jshint'),
  stylus = require('gulp-stylus'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  mocha = require('gulp-mocha');

// lint task
gulp.task('lint', function() {
  gulp.src([
    './**/*.js',
    '!./public/js/lib/**',
    '!./node_modules/**',
    '!./build/**/*.js'
  ])
    .pipe(jshint('./.jshintrc'))
    .pipe(jshint.reporter('jshint-stylish'));
});

// compile stylus to css
gulp.task('stylus', function() {
  gulp.src('./public/styles/*.styl')
    .pipe(stylus())
    // minify css??
    .pipe(rename('brim.min.css'))
    .pipe(gulp.dest('./build/styles'));
    // .pipe(refresh(server));
});

// copy files and folders
gulp.task('copy', function() {
  // images
  gulp.src('./public/img/**').pipe(gulp.dest('./build/img'));
  // js libraries
  gulp.src('./public/js/lib/**').pipe(gulp.dest('./build/js/lib'));
  // js utils
  gulp.src('./public/js/util/**').pipe(gulp.dest('./build/js/util'));
  // fonts
  gulp.src('./public/styles/fonts/**').pipe(gulp.dest('./build/styles/fonts'));
  // font-awesome
  gulp.src('./public/styles/fa/**').pipe(gulp.dest('./build/styles/fa'));
  // bootstrap
  gulp.src('./public/styles/btsp.min.css').pipe(gulp.dest('./build/styles'));
});

// concatenate and minify
gulp.task('scripts', function() {
  gulp.src(['./public/js/brim.js'])
    // .pipe(concat('brim.js'))
    // .pipe(gulp.dest('./build/js/'))
    .pipe(uglify())
    .pipe(rename('brim.min.js'))
    .pipe(gulp.dest('./build/js/'));
});

gulp.task('test', function() {
  gulp.src('./test/tickets.js')
    .pipe(mocha({
      reporter: 'spec'
    }));
  gulp.run('lint', 'test');
});

// default task
gulp.task('default', function() {
  gulp.run('lint', 'stylus', 'scripts');

  // watch for changes to our JS
  gulp.watch([
    './**/*.js',
    '!./node_modules/**',
    '!./build/**'
  ], function(event) {
    gulp.run('lint', 'test');
  });

  // watch for changes in css and static files
  gulp.watch('./public/**', function(event) {
    gulp.run('stylus', 'copy');
  });
});