// index.js

var nconf = require('nconf');

function Config() {
  var env;

  nconf.argv().env('_');
  env = nconf.get('NODE:ENV' || 'development');
  nconf.file(env, 'config/' + env + '.json');
  nconf.file('default', 'config/default.json');
}

Config.prototype.get = function(key) {
  return nconf.get(key);
};

module.exports = new Config();