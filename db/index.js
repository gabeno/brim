// index.js

var
  mongoose = require('mongoose'),
  config = require('../config');

// connect to mongodb
var DbURI = process.env.OPENSHIFT_MONGODB_DB_URL + process.env.OPENSHIFT_APP_NAME;
var connString = DbURI || config.get('mongo:url');
var options = {};

options = {
  server: {
    poolSize: 10
  }
};

mongoose.connect(connString, options);

// successfully connected
mongoose.connection.on('connected', function() {
  console.log('Connection to %s open', connString);
});

// connection throws error
mongoose.connection.on('error', function(err) {
  console.log('Connection error %s', err);
});

// connection disconnected
mongoose.connection.on('disconected', function() {
  console.log('Connection disconnected.');
});

// if node process ends, close disconnection
mongoose.connection.on('SIGINT', function() {
  mongoose.connection.close(function() {
    console.log('Connection terminated through app.');
    process.exit(0);
  });
});