
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var tickets = require('./routes/tickets');
var imap = require('./routes/imap');
var http = require('http');
var path = require('path');
var hbs = require('express-hbs');
var db = require('./db');
var validateId = require('./middleware').validateId;

// set up express server
var app = express();

var ipaddress = process.env.OPENSHIFT_INTERNAL_IP || process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var port = process.env.OPENSHIFT_INTERNAL_PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000;

// all environments
app.set('port', port);
app.set('views', __dirname + '/views');
app.engine('hbs', hbs.express3({
  partialsDir: __dirname + '/views/partials',
  layoutsDir: __dirname + '/views/layouts',
  contentHelperName: 'content'
}));
app.set('view engine', 'hbs');
app.use(express.favicon('dist/img/favicon.ico'));
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'build')));
app.param('tid', validateId);
app.param('nid', validateId);

// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
}

// set production constant
app.set('PROD_MODE', ('production' === app.get('env')));

// GET
app.get('/', routes.index);
app.get('/plans', routes.plans);
app.get('/choose', routes.choose);
app.get('/thankyou', routes.thankyou);
app.get('/inbox', imap.inbox);
app.get('/tickets', tickets.tickets);
app.get('/tickets/:tid', tickets.ticket);

// POST
app.post('/save', routes.save);
app.post('/tickets', tickets.createTicket);
app.post('/tickets/:tid/notes', tickets.createNote);
app.post('/tickets/:tid/notes/:nid/edit', tickets.editNote);

http.createServer(app).listen(app.get('port'), ipaddress, function(){
  var env = process.env.NODE_ENV || 'development';
  console.log('Express server listening on port ' + app.get('port') + ' env ' + env);
});

module.exports = app;