// mail.js
// 
// @todo: set email template system -> node-campaign
// @todo: persist new emails to db
// @roadmap: attachments?
// @todo: log success and failure.

'use strict';

var simplesmtp = require('simplesmtp'),
  nodemailer = require('nodemailer'),
  Imap = require('imap'),
  MailParser = require('mailparser').MailParser,
  Promise = require('bluebird');

// ********************************
//          Send Mail
// ********************************

function SendMail(config) {
  if (!(this instanceof SendMail))
    return new SendMail(config);
  
  this.transport = nodemailer.createTransport('SMTP', config);
}

SendMail.prototype.checkSmtpConn = function(config, cb) {
  var conn = simplesmtp.connect(config.port, config.host, config);

  conn.once('idle', function() {
    conn.removeAllListeners();
    conn.close();
    cb(null, true);
  });

  conn.once('error', function(err) {
    conn.removeAllListeners();
    conn.close();
    cb(err);
  });
};

SendMail.prototype.sendEmail = function(opts, cb) {
  this.transport.sendMail(opts, cb);
};

SendMail.prototype.closeSmtpConn = function(cb) {
  this.transport.close(cb);
};

module.exports.send = SendMail;




// ********************************
//          Read Mail
// ********************************

// @todo: close mailbox
function ReadMail(config) {
  if (!(this instanceof ReadMail))
    return new ReadMail(config);

  this.server = new Imap(config);
  this.data = [];

  var self = this;

  this.server.once('ready', function(err) {
    self.server.openBox('INBOX', true, function(err, box) {
      self._parseUnreadEmails();
      // check for new email
      self.server.on('mail', function(num) {
        self._parseUnreadEmails();
      });
    });
  });

  this.server.connect();
}

ReadMail.prototype.getEmails = function() {
  return this.data;
};

ReadMail.prototype._error = function(cb) {
  this.server.on('error', cb);
};

ReadMail.prototype._parseUnreadEmails = function() {
  var self = this;

  self.server.search(['UNSEEN'], function(err, msgIDs) {
    if (err) console.log(err);
    var msgCount = msgIDs.length;
    if (!msgCount) {
      console.log('Woohoo!, %d messages for you', msgCount);
    } else {
      var fetch = self.server.fetch(msgIDs, {
        bodies: '',
        markSeen: true
      });

      fetch.on('message', function(msgObj, uid) {
        msgObj.on('body', function(msgStream, msgInfo) {
          var parser = new MailParser();
          // console.log('msg id %d', uid);
          // self.data.push(uid);
          parser.on('end', function(data) {
            self.data.push(data); // = data
          });
          msgStream.pipe(parser); // handles both data and end events
        });
      });

      // after all messages read, run these functions
      fetch.on('end', function() {
        // close connection to server? No. Listen to incoming mails
        // this.server.end();
      });
    }
  });
};

module.exports.read = ReadMail;