// ticket.js

var
  Ticket = require('../models/ticket'),
  mongoose = require('mongoose');

function TicketRecipe() {}

TicketRecipe.prototype.newTicket = function(data, cb) {
  var ticket = new Ticket(data);

  // @todo make it more generic - remove { 'msgId': data.msgId } part.
  Ticket.findOne({ 'msgId': data.msgId }, function(err, doc) {
    if (err) return cb(err, null);
    if (doc !== null) return cb(null, null);
    ticket.save(function(err, res) {
      if (err) return cb(err, null);
      return cb(null, res);
    });
  });
};

TicketRecipe.prototype.tickets = function(cb) {
  Ticket.find({}, function(err, doc) {
    if (err) return cb(err, null);
    if (doc === null) return cb(null, null);
    return cb(null, doc);
  });
};

TicketRecipe.prototype.ticket = function(tid, cb) {
  Ticket.findOne({_id: tid}, function(err, doc) {
    if (err) return cb(err, null);
    if (doc === null) return cb(null, null);
    return cb(null, doc);
  });
};

TicketRecipe.prototype.createNote = function(tid, data, cb) {
  Ticket.findOneAndUpdate({_id: tid}, {$push: {notes: data}},
                          {select: 'notes'}, function(err, doc) {
    if (err) return cb(err, null);
    if (doc === null) return cb(null, null);
    return cb(null, doc);
  });
};

TicketRecipe.prototype.editNote = function(tid, nid, data, cb) {
  Ticket.findOneAndUpdate({_id: tid, 'notes._id': nid},
                          {$set: {'notes.$.text': data.text}},
                          {select: 'notes'},
                          function(err, doc) {
    if (err) return cb(err, null);
    if (doc === null) return cb(null, null);
    return cb(null, doc);
  });
};

module.exports = TicketRecipe;