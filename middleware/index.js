// id.js

exports.validateId = function(req, res, next, id) {
  if (id.match(/^[a-f\d]{24}$/i) === null)
    return res.json(400, 'Bad Request');
  next();
};