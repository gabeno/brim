process.env.NODE_ENV = 'test';

// @todo: add some code coverage - istanbul or blanket?
// @todo: add mocha-sinon plugin

var
  expect = require('chai').expect,
  sinon = require('sinon'),
  mail = require('../util/mail'),
  bluebird = require('bluebird'),
  settingsJSON = require('../settings.json'),
  chalk = require('chalk');


describe('Mail functions', function() {
  var opts, read, send, data;

  before(function() {
    // set email body options
    opts = {
      from: 'Brim Team <trybrim@gmail.com>',
      replyTo: 'gmajivu@gmail.com',
      bcc: 'gmajivu@gmail.com',
      generateTextFromHTML: true,
      to: 'gmajivu@gmail.com',
      subject: 'test',
      html: '<p>this is a test email.</p>'
    };

    // instantiate respective functions
    send = mail.send(settingsJSON.smtp);
    read = mail.read(settingsJSON.imap);
  });

  after(function(done) {
    send.closeSmtpConn(function() {
      console.log(chalk.cyan('    smtp server closed'));
      console.log(chalk.cyan('    imap server waiting for new message...'));
      done();
    });
  });

  describe('mail', function() {
    it('should have a constructor function sendMail()', function() {
      expect(mail.send).to.be.a('function');
      expect(send).to.be.an.instanceof(mail.send);
      expect(send.checkSmtpConn).to.be.a('function');
      expect(send.sendEmail).to.be.a('function');
      expect(send.closeSmtpConn).to.be.a('function');
    });

    it('should have a constructor function readMail()', function() {
      expect(mail.read).to.be.a('function');
      expect(read).to.be.an.instanceof(mail.read);
      expect(read.getEmails).to.be.a('function');
      expect(read._error).to.be.a('function');
      expect(read._parseUnreadEmails).to.be.a('function');
    });
  });

  describe('send mail via smtp', function() {
    this.timeout(15000);

    it('should connect to server', function(done) { // => ????
      send.checkSmtpConn(settingsJSON.smtp, function(err, res) {
        if (err) {
          done(err);
        } else {
          // console.log('Successs: ', res);
          done();
        }
      });
    });

    it('should send mail', function(done) {
      send.sendEmail(opts, function(err, res) {
        if (err) {
          done(err);
        } else {
          expect(res).to.be.an('object');
          expect(res.message).to.be.a('string');
          expect(res.messageId).to.be.a('string');
          expect(res.failedRecipients).to.be.empty;
          done();
        }
      });
    });
  });

  describe('read mail via imap', function() {
    this.timeout(15000);

    it('should fetch unread messages from the INBOX', function(done) {
      data = read.getEmails();
      // var msgCount = data.length;
      if (data) {
        expect(data).to.be.an('array');
        expect(data).have.length.above(0);
        console.log('%d message(s)', data.length);
        console.log(data);
      } else {
        expect(data).to.be.undefined;
      }
      done();
    });
  });
});