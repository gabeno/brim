// db.js

process.env.NODE_ENV = 'test';

var
  request = require('supertest'),
  mocha = require('mocha'),
  expect = require('chai').expect,
  app = require('../server'),
  mongoose = require('mongoose'),
  Chance = require('chance');

describe('Ticket API', function() {
  var id, note, noteId;
  var chance = new Chance();

  chance.mixin({
    'ticket': function() {
      return {
        channel: chance.pick(['email','twitter']),
        createdBy: chance.name(),
        msgId: chance.string(),
        cusName: chance.name(),
        cusContact: chance.email({domain: 'brim-test.com'}),
        title: chance.sentence({words: 5}),
        text: chance.paragraph({sentences: 2})
      };
    }
  });

  chance.mixin({
    'note': function() {
      return {
        text: chance.paragraph({sentences: 2}),
        user: chance.name()
      };
    }
  });

  note = chance.note();

  before(function(done) {
    // create a resource for each test
    this.timeout(5000);
    mongoose.connection.collections['tickets'].insert(chance.ticket(), function(err, doc) {
      if (err) done(err);
      id = doc[0]._id;
      done();
    });
  });

  after(function(done) {
    mongoose.connection.collections['tickets'].drop(function(err) {
      if (err) done(err); // ns error no collection first time
      done();
    });
  });

  describe('POST /tickets', function() {
    describe('when creating a new resource /tickets', function() {
      it('should respond with 201 for success',function(done) {
        var ticket = chance.ticket();
        request(app)
          .post('/tickets')
          .send(ticket)
          .expect(201, function(err, res) {
            expect(err).to.not.exist;
            expect(res.body).to.exist;
            expect(res.type).to.equal('application/json');
            expect(res.status).to.equal(201);
            expect(res.ok).to.be.true;
            expect(res.body._id).to.be.ok;
            expect(res.body.channel).to.equal(ticket.channel);
            expect(res.body.createdBy).to.equal(ticket.createdBy);
            expect(res.body.msgId).to.equal(ticket.msgId);
            expect(res.body.cusName).to.equal(ticket.cusName);
            expect(res.body.cusContact).to.equal(ticket.cusContact);
            expect(res.body.title).to.equal(ticket.title);
            expect(res.body.text).to.equal(ticket.text);
            done();
          });
      });
    });
  });

  describe('GET /tickets', function() {
    describe('when querying for all tickets in db', function() {
      it('should respond with 200', function(done) {
        request(app)
          .get('/tickets')
          .expect(200, function(err, res) {
            expect(err).to.not.exist;
            expect(res.body).to.exist;
            expect(res.body.length).to.be.above(1);
            expect(res.type).to.equal('application/json');
            done();
          });
      });
    });
  });

  describe('GET /tickets/:id', function() {
    describe('when querying for a ticket', function() {
      it('should respond with 200', function(done) {
        request(app)
          .get('/tickets/'+id)
          .expect(200, function(err, res) {
            expect(err).to.not.exist;
            expect(res.body).to.exist;
            expect(res.type).to.equal('application/json');
            done();
          });
      });

      it('should respond with 200', function(done) {
        request(app)
          .get('/tickets/456104703412326920000002')
          .expect(404, function(err, res) {
            expect(err).to.not.exist;
            expect(res.body).to.exist;
            expect(res.type).to.equal('application/json');
            done();
          });
      });

      it('should respond with 200', function(done) {
        request(app)
          .get('/tickets/45cbc4a0e4123f6920000002')
          .expect(404, function(err, res) {
            expect(err).to.not.exist;
            expect(res.body).to.exist;
            expect(res.type).to.equal('application/json');
            done();
          });
      });
    });
  });

  describe('POST /tickets/:id/notes', function() {
    describe('valid ticket valid id', function() {
      // request processed, no content returned
      it('should respond with 201', function(done) {
        request(app)
          .post('/tickets/' + id + '/notes')
          .send(note)
          .expect(201, function(err, res) {
            noteId = res.body.notes[0]._id;
            // console.log(res.body);
            expect(res.body.notes).to.exist;
            expect(err).to.not.exist;
            done();
          });
      });
    });

    describe('invalid ticket with valid id - not existing', function() {
      // requested resource could not be found
      it('should respond with 404', function(done) {
        request(app)
          .post('/tickets/45cbc4a0e4123f6920000002/notes')
          .send(note)
          .expect(404, done);
      });
    });

    describe('invalid ticket with valid id format - all numbers', function() {
      it('should respond with 404', function(done) {
        request(app)
          .post('/tickets/456104703412326920000002/notes')
          .send(note)
          .expect(404, done);
      });
    });

    describe('invalid ticket invalid id format - all letters', function() {
      // request can not be fulfilled due to bad syntax
      it('should respond with 400', function(done) {
        request(app)
          .post('/tickets/bad_id*tgvedyojTMnabbEEj/notes')
          .send(note)
          .expect(400, done);
      });
    });
  });

  describe('POST /tickets/:id/notes/:id/edit', function() {
    describe('edit a note', function() {
      var note = chance.note();
      it('should respond with 201', function(done) {
        request(app)
          .post('/tickets/'+ id +'/notes/'+ noteId +'/edit')
          .send(note)
          .expect(201, function(err, res) {
            // console.log(res.body);
            expect(res.body.notes).to.exist;
            expect(err).to.not.exist;
            done();
          });
      });
    });
  });
});