// email model

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var channels = 'email twitter sms'.split(' ');

var noteSchema = new Schema({
  user: { type: String },
  text: { type: String, trim: true },
  created: { type: Date, default: Date.now }
});

var ticketSchema = new Schema({
  channel: { type: String, enum: channels },
  createdAt: { type: Date, default: Date.now },
  createdBy: { type: String, required: true }, // Schema.objetId
  msgId: { type: String, unique: true, index: true, },
  cusName: { type: String, required: true, trim: true },
  cusContact: { type: String, required: true, trim: true },
  title: { type: String, required: true, trim: true },
  text: { type: String, required: true, trim: true },
  notes: [noteSchema]
}, { collection: 'tickets' });

var Ticket = mongoose.model('Ticket', ticketSchema);

module.exports = Ticket;

// shorter names to save on space
// txt => text
// sub => subject
// mid => msgId
// mdt => msgDate
// fro => from
// to => to
// cc => cc
// nts => notes
// ctd => createdAt
// adr => address
// nom => name